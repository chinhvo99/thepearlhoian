interface Box {
  height: number;
  width: number;
}

interface Box {
  scale: number;
  width: number;
}

let box: Box = { height: 5, width: 6, scale: 10 };
